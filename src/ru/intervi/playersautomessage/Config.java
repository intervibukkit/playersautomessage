package ru.intervi.playersautomessage;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.ConcurrentHashMap;
import java.util.List;
import java.util.Map.Entry;
import java.util.UUID;
import java.util.Timer;
import java.util.TimerTask;

import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.configuration.ConfigurationSection;

public class Config {
	public Config(Main main) {
		this.main = main;
		PATH = main.getDataFolder().getAbsoluteFile() + File.separator + "messages.yml";
	}
	
	private Main main;
	private final String PATH;
	private Timer timer = null;
	
	public volatile ConcurrentHashMap<UUID, ArrayList<String>> messages = new ConcurrentHashMap<UUID, ArrayList<String>>();
	public volatile ConcurrentHashMap<UUID, Long> times = new ConcurrentHashMap<UUID, Long>();
	
	public boolean enable = true;
	public boolean broadcast = true;
	public String bmess = "";
	public String mess = ""; //%name%, %mess%
	public int cost = 1000;
	public int renew = 500;
	public int scale = 4;
	public int time = 60; //min
	public int limit = 1;
	public int glimit = 10;
	public int bint = 180; //sec
	public int mint = 300; //sec
	public int save = 300; //sec
	
	public String reload = "";
	public String clear = "";
	public String saved = "";
	public String noperm = "";
	public String nomoney = ""; //%cost%
	public String limited = ""; //%limit%
	public String glimited = ""; //%limit%
	public String endtime = "";
	public String error = "";
	public String added = ""; //%time%
	public String renewed = ""; //%time%
	public String listop = ""; //%page%, %pages%
	public String edited = "";
	public String onlygame = "";
	public String removed = "";
	public String nomess = "";
	public String left = ""; //%time%
	public List<String> help = new ArrayList<String>();
	
	static String color(String str) {
		return ChatColor.translateAlternateColorCodes('&', str);
	}
	
	private static List<String> color(List<String> list) {
		ArrayList<String> result = new ArrayList<String>();
		for (String s : list) result.add(color(s));
		return result;
	}
	
	public void load() {
		main.saveDefaultConfig();
		main.reloadConfig();
		FileConfiguration conf = main.getConfig();
		enable = conf.getBoolean("enable");
		broadcast = conf.getBoolean("broadcast");
		bmess = color(conf.getString("bmess"));
		mess = color(conf.getString("mess"));
		cost = conf.getInt("cost");
		renew = conf.getInt("renew");
		scale = conf.getInt("scale");
		time = conf.getInt("time");
		limit = conf.getInt("limit");
		glimit = conf.getInt("glimit");
		bint = conf.getInt("bint");
		mint = conf.getInt("mint");
		save = conf.getInt("save");
		reload = color(conf.getString("reload"));
		clear = color(conf.getString("clear"));
		saved = color(conf.getString("saved"));
		noperm = color(conf.getString("noperm"));
		nomoney = color(conf.getString("nomoney"));
		limited = color(conf.getString("limited"));
		glimited = color(conf.getString("glimited"));
		endtime = color(conf.getString("endtime"));
		error = color(conf.getString("error"));
		added = color(conf.getString("added"));
		renewed = color(conf.getString("renewed"));
		listop = color(conf.getString("listop"));
		edited = color(conf.getString("edited"));
		onlygame = color(conf.getString("onlygame"));
		removed = color(conf.getString("removed"));
		nomess = color(conf.getString("nomess"));
		left = color(conf.getString("left"));
		help = color(conf.getStringList("help"));
	}
	
	public void loadDB() {
		File file = new File(PATH);
		if (!file.isFile()) {
			try {file.createNewFile();}
			catch(IOException e) {e.printStackTrace();}
		}
		FileConfiguration db = YamlConfiguration.loadConfiguration(file);
		for (String key : db.getKeys(false)) {
			UUID uuid = UUID.fromString(key);
			ArrayList<String> list = new ArrayList<String>();
			list.addAll(db.getConfigurationSection(key).getStringList("messages"));
			Long time = db.getConfigurationSection(key).getLong("time");
			if (messages.containsKey(uuid)) messages.replace(uuid, list);
			else messages.put(uuid, list);
			if (times.containsKey(uuid)) times.replace(uuid, time);
			else times.put(uuid, time);
		}
	}
	
	public void save() {
		File file = new File(PATH);
		if (!file.isFile()) {
			try {file.createNewFile();}
			catch(IOException e) {e.printStackTrace();}
		}
		FileConfiguration db = new YamlConfiguration();
		for (Entry<UUID, ArrayList<String>> entry : messages.entrySet()) {
			String key = entry.getKey().toString();
			ConfigurationSection sec = db.createSection(key);
			sec.set("messages", entry.getValue());
			if (times.containsKey(entry.getKey())) sec.set("time", times.get(entry.getKey()).longValue());
		}
		try {db.save(PATH);}
		catch(IOException e) {e.printStackTrace();}
	}
	
	public void clear() {
		messages.clear();
		times.clear();
	}
	
	public void startTimer() {
		timer = new Timer();
		timer.schedule(new Saver(), save*1000, save*1000);
	}
	
	public void stopTimer() {
		if (timer != null) {
			timer.cancel();
			timer = null;
		}
	}
	
	private class Saver extends TimerTask {
		@Override
		public void run() {
			save();
		}
	}
}
