package ru.intervi.playersautomessage;

import net.milkbowl.vault.Vault;
import net.milkbowl.vault.economy.Economy;
import net.milkbowl.vault.economy.EconomyResponse;
import net.milkbowl.vault.economy.EconomyResponse.ResponseType;

import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.command.CommandSender;
import org.bukkit.command.Command;
import org.bukkit.entity.Player;
import org.bukkit.OfflinePlayer;
import org.bukkit.Bukkit;

import java.text.MessageFormat;
import java.util.Timer;
import java.util.TimerTask;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;
import java.util.regex.Pattern;

public class Main extends JavaPlugin implements Listener {
	private Config conf = new Config(this);
	private Economy eco = null;
	private Timer timer = null;
	private Sender sender = null;
	
	@Override
	public void onEnable() {
		if(Bukkit.getPluginManager().getPlugin("Vault") instanceof Vault) {
        	RegisteredServiceProvider<Economy> service = Bukkit.getServicesManager().getRegistration(net.milkbowl.vault.economy.Economy.class);
            if(service != null) eco = service.getProvider();
            else {
            	getLogger().warning("Vault not found");
            	return;
            }
		}
		conf.load();
		conf.loadDB();
		conf.startTimer();
		startTimer();
	}
	
	@Override
	public void onDisable() {
		stopTimer();
		conf.stopTimer();
		conf.save();
		conf.clear();
	}
	
	private void startTimer() {
		timer = new Timer();
		sender = new Sender();
		timer.schedule(sender, 1000, 1000);
	}
	
	private void stopTimer() {
		if (timer != null) {
			timer.cancel();
			timer = null;
		}
		if (sender != null) sender = null;
	}
	
	private class Sender extends TimerTask {
		private int p = 0, bs = 0, ms = 0;
		private ConcurrentHashMap<UUID, Integer> map = new ConcurrentHashMap<UUID, Integer>();
		
		@Override
		public void run() {
			try {
				work();
			} catch(Exception e) {e.printStackTrace();}
		}
		
		private void work() {
			if (bs >= conf.bint) {
				Bukkit.broadcast(conf.bmess, "playersam.receive");
				bs = 0;
			} else bs++;
			if (ms < conf.mint) {
				ms++;
				return;
			} else ms = 0;
			long time = System.currentTimeMillis();
			int i = 0;
			for (Entry<UUID, ArrayList<String>> entry : conf.messages.entrySet()) {
				if (i < p) {
					i++;
					continue;
				}
				if (p + 1 < conf.messages.size()) p++;
				else p = 0;
				OfflinePlayer oplayer = Bukkit.getOfflinePlayer(entry.getKey());
				String name = "";
				if (oplayer != null) name = oplayer.getName();
				if (conf.times.containsKey(entry.getKey())) {
					long min = (time/1000/60) - (conf.times.get(entry.getKey())/1000/60);
					if (min > conf.time) {
						conf.messages.remove(entry.getKey());
						conf.times.remove(entry.getKey());
						map.remove(entry.getKey());
						Player player = Bukkit.getPlayer(entry.getKey());
						if (player != null && player.isOnline()) player.sendMessage(conf.endtime);
						continue;
					}
				}
				if (entry.getValue() == null || entry.getValue().isEmpty()) {
					conf.messages.remove(entry.getKey());
					conf.times.remove(entry.getKey());
					map.remove(entry.getKey());
					break;
				}
				int m = 0;
				if (map.containsKey(entry.getKey())) m = map.get(entry.getKey());
				if (m >= entry.getValue().size()) m = 0;
				String mess = Pattern.quote(entry.getValue().get(m));
				mess = mess.substring(2, mess.length() - 2).trim();
				String msg = MessageFormat.format(conf.mess, name, mess);
				Bukkit.broadcast(msg, "playersam.receive");
				m++;
				if (map.containsKey(entry.getKey())) map.replace(entry.getKey(), Integer.valueOf(m));
				else map.put(entry.getKey(), Integer.valueOf(m));
				break;
			}
		}
		
		void remove(UUID uuid) {
			map.remove(uuid);
		}
		
		void clear() {
			map.clear();
		}
	}
	
	private boolean take(Player player, int cost) {
		if (eco.has(player, cost)) {
			if (!player.hasPermission("playersam.nocost")) {
				EconomyResponse er = eco.withdrawPlayer(player, cost);
				ResponseType type = er.type;
				if (!type.equals(ResponseType.SUCCESS)) {
					getLogger().warning("Vault error withdraw from " + player.getName() + ' ' + type.toString() + ": " + er.errorMessage);
					player.sendMessage(conf.error);
					return false;
				}
			}
		} else {
			player.sendMessage(conf.nomoney.replaceAll("%cost%", String.valueOf(conf.cost)));
			return false;
		}
		return true;
	}
	
	private UUID getUUID(boolean isplayer, String args[], String perm, CommandSender sender) {
		UUID uuid = null;
		if (!isplayer) {
			if (args.length >= 2) {
				try {uuid = UUID.fromString(args[1]);}
				catch(Exception e) {
					Player player = Bukkit.getPlayer(args[1]);
					if (player != null) uuid = player.getUniqueId(); else {
						sender.sendMessage(conf.error);
						return null;
					}
				}
			} else {
				for (String s : conf.help) sender.sendMessage(s);
				return null;
			}
		} else {
			if (args.length >= 2) {
				try {uuid = UUID.fromString(args[1]);}
				catch(Exception e) {
					Player player = Bukkit.getPlayer(args[1]);
					if (player != null) uuid = player.getUniqueId(); else {
						sender.sendMessage(conf.error);
						return null;
					}
				}
				if (uuid != null && (!sender.hasPermission(perm) || !((Player) sender).getUniqueId().equals(uuid))) {
					sender.sendMessage(conf.noperm);
					return null;
				}
			} else uuid = ((Player) sender).getUniqueId();
		}
		return uuid;
	}
	
	private ArrayList<String> getMess(List<String> list, int page) {
		ArrayList<String> result = new ArrayList<String>();
		result.add(conf.listop.replaceAll("%page%", String.valueOf(page)).replaceAll("%pages%", String.valueOf((int) Math.ceil((list.size()/10)))));
		if (list.isEmpty()) return result;
		int to = page*10-1;
		if (to >= list.size()) to = list.size()-1;
		int from = to-9;
		if (from < 0) from = 0;
		for (int i = from; i <= to; i++) result.add(list.get(i));
		return result;
	}
	
	private ArrayList<String> getMess(UUID uuid, int page) {
		if (!conf.messages.containsKey(uuid)) return null;
		else return getMess(conf.messages.get(uuid), page);
	}
	
	private double round(double number) { //from mutagen in cyberforum
        int pow = 10;
        for (int i = 1; i < conf.scale; i++)
            pow *= 10;
        double tmp = number * pow;
        return (double) (int) ((tmp - (int) tmp) >= 0.5f ? tmp + 1 : tmp) / pow;
    }
	
	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if (args == null || args.length == 0) {
			for (String s : conf.help) sender.sendMessage(s);
			return true;
		}
		boolean isplayer = false;
		if (sender instanceof Player) isplayer = true;
		switch(args[0].toLowerCase()) {
		case "reload":
			if (sender.hasPermission("playersam.reload")) {
				conf.load();
				stopTimer();
				startTimer();
				sender.sendMessage(conf.reload);
			} else sender.sendMessage(conf.noperm);
			break;
		case "reload-db":
			if (sender.hasPermission("playersam.reload")) {
				conf.loadDB();
				sender.sendMessage(conf.reload);
			} else sender.sendMessage(conf.noperm);
			break;
		case "clear":
			if (sender.hasPermission("playersam.clear")) {
				conf.clear();
				if (this.sender != null) this.sender.clear();
				sender.sendMessage(conf.clear);
			} else sender.sendMessage(conf.noperm);
			break;
		case "save":
			if (sender.hasPermission("playersam.save")) {
				conf.save();
				sender.sendMessage(conf.saved);
			} else sender.sendMessage(conf.noperm);
			break;
		case "add":
			if (sender.hasPermission("playersam.add")) {
				if (!isplayer) {
					sender.sendMessage(conf.onlygame);
					break;
				}
				if (args.length < 2) {
					for (String s : conf.help) sender.sendMessage(s);
					return true;
				}
				if (conf.messages.size() >= conf.glimit) {
					sender.sendMessage(conf.glimited.replaceAll("%limit%", String.valueOf(conf.glimit)));
					break;
				}
				ArrayList<String> list = new ArrayList<String>();
				UUID uuid = ((Player) sender).getUniqueId();
				if (conf.messages.containsKey(uuid)) list.addAll(conf.messages.get(uuid));
				if (list.size() >= conf.limit) {
					sender.sendMessage(conf.limited.replaceAll("%limit%", String.valueOf(conf.limit)));
					break;
				}
				if (!take((Player) sender, conf.cost)) break;
				String mess = args[1];
				for (int i = 2; i < args.length; i++) mess += ' ' + args[i];
				list.add(Config.color(mess));
				if (conf.messages.containsKey(uuid)) conf.messages.replace(uuid, list);
				else conf.messages.put(uuid, list);
				Long time = System.currentTimeMillis();
				if (conf.times.containsKey(uuid)) conf.times.replace(uuid, time);
				else conf.times.put(uuid, time);
				sender.sendMessage(conf.added.replaceAll("%time%", String.valueOf(conf.time)));
			} else sender.sendMessage(conf.noperm);
			break;
		case "del":
			if (sender.hasPermission("playersam.del") || sender.hasPermission("playersam.del-all")) {
				UUID uuid = null;
				uuid = getUUID(isplayer, args, "playersam.del-all", sender);
				if (uuid == null) break;
				conf.messages.remove(uuid);
				conf.times.remove(uuid);
				if (this.sender != null) this.sender.remove(uuid);
				sender.sendMessage(conf.removed);
			} else sender.sendMessage(conf.noperm);
			break;
		case "renew":
			if (sender.hasPermission("playersam.renew") || sender.hasPermission("playersam.renew-all")) {
				UUID uuid = null;
				uuid = getUUID(isplayer, args, "playersam.renew-all", sender);
				if (uuid == null) break;
				if (conf.times.containsKey(uuid)) {
					if (!take((Player) sender, conf.renew)) break;
					double time = (double) conf.time - ((double) ((double) System.currentTimeMillis()/1000/60) - ((double) conf.times.get(uuid)/1000/60));
					time = round(conf.time - time);
					conf.times.replace(uuid, Long.valueOf(System.currentTimeMillis()));
					sender.sendMessage(conf.renewed.replaceAll("%time%", String.valueOf(time)));
				} else sender.sendMessage(conf.nomess);
			} else sender.sendMessage(conf.noperm);
			break;
		case "edit":
			if (sender.hasPermission("playersam.edit") || sender.hasPermission("playersam.edit-all")) {
				UUID uuid = null;
				uuid = getUUID(isplayer, args, "playersam.edit-all", sender);
				if (uuid == null) break;
				int page = 1, mess = 0;
				boolean l = false;
				if (args.length >= 3) {
					if (args[2].matches("^[0-9]*$")) page = Integer.parseInt(args[2]);
					else {
						for (String s : conf.help) sender.sendMessage(s);
						break;
					}
					if (page <= 0) page = 1;
				} else l = true;
				if (args.length >= 4) {
					if (args[3].matches("^[0-9]*$")) mess = Integer.parseInt(args[3]);
					else {
						for (String s : conf.help) sender.sendMessage(s);
						break;
					}
					if (mess < 0) mess = 0;
				} else l = true;
				if (l) {
					List<String> list = getMess(uuid, page);
					if (list == null || list.size() == 1) {
						sender.sendMessage(conf.nomess);
						break;
					}
					for (byte i = 1; i < 11 && i < list.size(); i++)
						list.set(i, '[' + String.valueOf(page == 1 ? -1+i : page*10-2+i) + "] " + list.get(i));
					for (String s : list) sender.sendMessage(s);
					break;
				}
				if (conf.messages.containsKey(uuid)) {
					ArrayList<String> list = conf.messages.get(uuid);
					if (mess >= list.size()) {
						sender.sendMessage(conf.error);
						break;
					}
					if (args.length < 5) list.remove(mess);
					else list.set(mess, args[4]);
					conf.messages.replace(uuid, list);
					sender.sendMessage(conf.edited);
				} else sender.sendMessage(conf.nomess);
			} else sender.sendMessage(conf.noperm);
			break;
		case "time":
			if (sender.hasPermission("playersam.time") || sender.hasPermission("playersam.time-all")) {
				UUID uuid = null;
				uuid = getUUID(isplayer, args, "playersam.time-all", sender);
				if (uuid == null) break;
				if (!conf.times.containsKey(uuid)) {
					sender.sendMessage(conf.nomess);
					break;
				}
				double time = round((double) conf.time - ((double) ((double) System.currentTimeMillis()/1000/60) - ((double) conf.times.get(uuid)/1000/60)));
				sender.sendMessage(conf.left.replaceAll("%time%", String.valueOf(time)));
			} else sender.sendMessage(conf.noperm);
			break;
		case "mess":
			if (sender.hasPermission("playersam.mess")) {
				int page = 1;
				if (args.length >= 2) {
					if (args[1].matches("^[0-9]*$")) page = Integer.parseInt(args[1]);
					else {
						for (String s : conf.help) sender.sendMessage(s);
						break;
					}
					if (page <= 0) page = 1;
				}
				ArrayList<String> list = new ArrayList<String>();
				for (Entry<UUID, ArrayList<String>> entry : conf.messages.entrySet()) {
					String key = entry.getKey().toString();
					OfflinePlayer oplayer = Bukkit.getOfflinePlayer(entry.getKey());
					if (oplayer != null) key = oplayer.getName();
					for (String s : entry.getValue()) list.add('[' + key + "]: " + s);
				}
				for (String s : getMess(list, page)) sender.sendMessage(s);
			} else sender.sendMessage(conf.noperm);
			break;
		case "help":
			for (String s : conf.help) sender.sendMessage(s);
			break;
		default:
			return false;
		}
		return true;
	}
}
